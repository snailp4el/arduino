////plan
//button demo + 
//led before fiting

#include <Servo.h>
//servo
Servo myservo;  // create servo object to control a servo

//pins
int ledBlue = 13;
int ledGreen = 8;
int ledWhite = 7;
int servo = 9;
int button = 2;

int SEC_IN_H = 3600;
long servoTiming;
int servoDelay = 20;
int servoPosition = 300;

long blueTiming;
boolean blueIsUp = false;

// buttons 
boolean buttonDown = false;

//a timer for the fiter
int timers[10] = {
  9, 0, 0, 0, 0, 0, 0, 0, 0, 0
};
int servoFullOpen = 155; //max 180 если открывать на полную выпадет максимум корма



boolean timerB = true;

int ledMinBeforFiting = 30; 
//sek
long sekTiming = 1000;
long sek = 0;


boolean buttonPresed = false;

long startTiming;


    //led blink
    boolean greenUpNow = true;
    int greenR = 0;
    int saveGreenR = 0;
    int upTime = 300;
    int extraUpTime = 3000;
    long greenTiming;


void setup() {
  Serial.begin(9600); //установка Serial-порта на скорость 9600 бит/сек
  Serial.println("setup");
  
  pinMode(button ,INPUT); // кнопка на пине 2
  digitalWrite(button ,HIGH); // подключаем встроенный подтягивающий резистор

  
  pinMode(ledGreen, OUTPUT); //led
  pinMode(ledWhite, OUTPUT); //led
  pinMode(ledBlue, OUTPUT); //led

  startTiming = millis();
}


void loop() {
      sek = millis()/1000;// для отладки можно поменять тысячу на 10

      //print timer
      if (millis() - sekTiming > 1000){
        //Serial.print("millis - ");
        //Serial.println(millis(), DEC);
        Serial.print("time - ");
        
        Serial.print(sek/60/60, DEC);//hour
        Serial.print(":");
        Serial.print((sek/60) - (sek/60/60)*60, DEC);//min
        Serial.print(":");
        Serial.println(sek-(sek/60)*60, DEC);//sec
        sekTiming = (millis()/1000)*1000;    
      }

      
      //chek timers array and start it time
            for(int i = 0; i < 10; i++){
                if (timers[i] != 0 && sek > timers[i]*SEC_IN_H){
              Serial.println("+++++++timer++++++++");
              Serial.println(timers[i]);
              Serial.println("+++++++timer++++++++");
              myservo.attach(servo);  // attaches the servo on pin 9 to the servo object
              servoPosition = 0;
              timers[i] = 0;
            } 

               if (timers[i] != 0 && sek >= (timers[i]*SEC_IN_H) - ledMinBeforFiting * 60  ){
                    

                    
              long l = ((timers[i]*SEC_IN_H)  - sek)/60;// минут до кормления

              if(millis() - blueTiming > l*100){
                blueIsUp = !blueIsUp;
                digitalWrite(ledBlue, blueIsUp);
                blueTiming = millis();
              }
              
              if ( l <= 5 ){digitalWrite(ledGreen, HIGH);}
              //Serial.println(l);
 
            }

            
        }

    greenController();
    servoStart();
    checkTheButton();

    
    //button
    if (buttonPresed){

              myservo.attach(servo);  // attaches the servo on pin 9 to the servo object
              servoPosition = 10;
    }
    
    
 }
    
  
//----------metods--------------`


   long buttonTiming;
   boolean buttonPresed1 = false;
   
   void checkTheButton(){
    if(millis() - buttonTiming < 20){return;}
    buttonTiming = millis();
    //logik

    boolean  bu = !digitalRead(button);

    if (bu == buttonPresed1){
       buttonPresed = bu;
    }
     if (buttonPresed){
     Serial.println(buttonPresed); 
     }
    
    buttonPresed1 = bu;
   }

  


  void greenController(){

    if(greenR == 0){
      return;
    }
    int extra =0;

    if ( greenR == 1){
      extra = extraUpTime;
    }
    
    if(millis() - greenTiming > upTime + extra){
      digitalWrite(ledGreen, greenUpNow);
      greenUpNow = !greenUpNow;
      greenR--;
      greenTiming = millis();
    }

        if(greenR == 0){
      greenR = saveGreenR;
      greenUpNow = true;
    }

  }



  void servoStart(){
    
    if (servoPosition > 200){
      myservo.detach();
      return;
      }
    if (millis() - servoTiming < servoDelay){return;}


    
    if (servoPosition <= servoFullOpen){
      digitalWrite(ledGreen, HIGH);
      digitalWrite(ledWhite, HIGH);
      
      myservo.write(servoPosition);
    }else{
      myservo.write(10);
      greenR = 0;
      saveGreenR = 0;
      digitalWrite(ledGreen, LOW);
      digitalWrite(ledWhite, LOW);
    }
    
    //Serial.println(servoPosition);
    servoTiming = millis();
    servoPosition = servoPosition +1;
  }



  void lightTheGreen(int howLong){

    digitalWrite(ledGreen, HIGH);

    delay(howLong);

    digitalWrite(ledGreen, LOW);

    delay(howLong);
    
  }

    void lightTheWhite(int howLong){

    digitalWrite(ledWhite, HIGH);

    delay(howLong);

    digitalWrite(ledWhite, LOW);

    delay(howLong);
    
  }
  

