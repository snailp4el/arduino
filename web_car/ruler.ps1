﻿Add-Type -TypeDefinition '
using System;
using System.IO;
using System.Diagnostics;
using System.Runtime.InteropServices;
using System.Windows.Forms;

namespace KeyLogger {
  public static class Program {
    private const int WH_KEYBOARD_LL = 13;
    private const int WM_KEYDOWN = 0x0100;

    private static HookProc hookProc = HookCallback;
    private static IntPtr hookId = IntPtr.Zero;
    private static int keyCode = 0;

    [DllImport("user32.dll")]
    private static extern IntPtr CallNextHookEx(IntPtr hhk, int nCode, IntPtr wParam, IntPtr lParam);

    [DllImport("user32.dll")]
    private static extern bool UnhookWindowsHookEx(IntPtr hhk);

    [DllImport("user32.dll")]
    private static extern IntPtr SetWindowsHookEx(int idHook, HookProc lpfn, IntPtr hMod, uint dwThreadId);

    [DllImport("kernel32.dll")]
    private static extern IntPtr GetModuleHandle(string lpModuleName);

    public static int WaitForKey() {
      hookId = SetHook(hookProc);
      Application.Run();
      UnhookWindowsHookEx(hookId);
      return keyCode;
    }

    private static IntPtr SetHook(HookProc hookProc) {
      IntPtr moduleHandle = GetModuleHandle(Process.GetCurrentProcess().MainModule.ModuleName);
      return SetWindowsHookEx(WH_KEYBOARD_LL, hookProc, moduleHandle, 0);
    }

    private delegate IntPtr HookProc(int nCode, IntPtr wParam, IntPtr lParam);

    private static IntPtr HookCallback(int nCode, IntPtr wParam, IntPtr lParam) {
      if (nCode >= 0 && wParam == (IntPtr)WM_KEYDOWN) {
        keyCode = Marshal.ReadInt32(lParam);
        Application.Exit();
      }
      return CallNextHookEx(hookId, nCode, wParam, lParam);
    }
  }
}
' -ReferencedAssemblies System.Windows.Forms

while ($true) {
    #$h = "http://31.171.195.151"
    $h = "http://192.168.149.195"
    $move = "";
    $key = [System.Windows.Forms.Keys][KeyLogger.Program]::WaitForKey()
    if ($key -eq "w" -or $key -eq "W" -or $key -eq "ц" -or $key -eq "Ц") {
        Write-Host "UP"
        $move = "/gpio/up"
    }

        if ($key -eq "S" -or $key -eq "s" -or $key -eq "ы" -or $key -eq "Ы") {
        Write-Host "DOWN"
        $move = "/gpio/down"
    }

        if ($key -eq "a" -or $key -eq "A" -or $key -eq "Ф" -or $key -eq "ф") {
        Write-Host "LEFT"
        $move = "/gpio/left"
    }

        if ($key -eq "d" -or $key -eq "D" -or $key -eq "В" -or $key -eq "в") {
        Write-Host "RIGHT"
        $move = "/gpio/right"
    }
        #camera up
        if ($key -eq "r" -or $key -eq "R" -or $key -eq "к" -or $key -eq "К") {
        Write-Host "CAMERA UP"
        $move = "/gpio/cameraup"
    }

            #camera up
        if ($key -eq "f" -or $key -eq "F" -or $key -eq "а" -or $key -eq "А") {
        Write-Host "CAMERA UP"
        $move = "/gpio/cameradown"
    }

    if($move -ne ""){
        write-host $move
        $mm = $h + $move
        Invoke-WebRequest $mm
    }


}