#include <ESP8266WiFi.h>
#include <Servo.h>

const char* ssid = "uley";
const char* password = "snailp4el";
//move pins
int rightForward = D7;
int leftForward = D6;
int rightBack = D9;
int leftBack = D8;

int cameraAngle;

long upTime;
long downTime;
long leftTime;
long rightTime;

String ip;

  // Пин для сервопривода
  int servoPin = 14;
  // Создаем объект
  Servo servo;
  int maxAngle = 155;
  int minAngle = 20;
  int servoStep = 30;
  int servoDelay = 30;

// Create an instance of the server
// specify the port to listen on as an argument
WiFiServer server(80);
void setup() {
  //servo
  servo.attach(servoPin);

  cameraAngle = 90;
  
  Serial.begin(9600);
  delay(10);

  
  //подготовка выходов
  pinMode(rightForward, OUTPUT);
  digitalWrite(rightForward, 0);
  pinMode(rightBack, OUTPUT);
  digitalWrite(rightBack, 0);
  pinMode(leftForward, OUTPUT);
  digitalWrite(leftForward, 0);
  pinMode(leftBack, OUTPUT);
  digitalWrite(leftBack, 0);
  
  // Connect to WiFi network
  Serial.println();
  Serial.println();
  Serial.print("Connecting to ");
  Serial.println(ssid);
  
  WiFi.begin(ssid, password);
  
  while (WiFi.status() != WL_CONNECTED) {
    delay(500);
    Serial.print(".");
  }
  Serial.println("");
  Serial.println("WiFi connected");
  
  //Запуск сервера
  server.begin();
  Serial.println("Server started");
  //выводим IP адрес в монитор порта
  Serial.println(WiFi.localIP());
  ip = ipToString(WiFi.localIP());
}
void loop() {


  if(millis() < upTime){
    moveUp();
  }else if(millis() < downTime){
    moveDown();
  }else if(millis() < leftTime){
    moveLeft();
  }else if(millis() < rightTime){
    moveRight();
  }else{
    moveStop();
  };


  
  //проверяем подключился ли клиент
  WiFiClient client = server.available();
  if (!client) {
    return;
  }
  
  //Ожидаем пока клиент не пришлет какие-нибудь данные
  String myString = String(millis());
  Serial.println("new client "+ myString);
  while(!client.available()){
    delay(1);
  }
  
  //Чтение первой строки запроса
  String req = client.readStringUntil('\r');
  Serial.println(req);
  client.flush();
  
  if (req.indexOf("/gpio/up") != -1){
    allTimeToNull();
    upTime = millis() + 500;
    
    }
    
  else if (req.indexOf("/gpio/down") != -1){
    allTimeToNull();
    downTime = millis() + 500;
    
    }
    //CAMERAUP
   else if (req.indexOf("/gpio/cameraup") != -1){
    changeCameAngle2(true);
    
    }//CAMERADOWN
    else if (req.indexOf("/gpio/cameradown") != -1){
    Serial.println("changeCameAngle " + cameraAngle);
    changeCameAngle2(false);
    
    }
  else if (req.indexOf("/gpio/left") != -1){
    allTimeToNull();
    leftTime = millis() + 200;
    }  
  else if (req.indexOf("/gpio/right") != -1){
    allTimeToNull();
    rightTime = millis() + 200;
    }    
  else {
    Serial.println("invalid request");
  }
  
  client.flush();
  // подготовка к ответу !!!!! тут надо заменить IP на полученный по dhcp
  String s = "HTTP/1.1 200 OK\r\nContent-Type: text/html\r\n\r\n<!DOCTYPE HTML>\r\n<html>\r\n ";
  
  s += "<center><br><a href='http://";
  s += ip;
  s += "/gpio/up'>UP</a><br>";
  
  s += "<br><a href='http://";
  s += ip;
  s +="/gpio/left'>LEFT</a>&emsp;&emsp;&emsp;&emsp;&emsp;";
  
  s += "<a href='http://";
  s += ip;
  s += "/gpio/right'>RIGHT</a><br>";
  
  s += "<br><a href='http://";
  s += ip;
  s += "/gpio/down'>DOWN</a>";

  s += "<br><a href='http://";
  s += ip;
  s += "/gpio/cameraup'>CAMERA_UP</a>";

  s += "<br><a href='http://";
  s += ip;
  s += "/gpio/cameradown'>CAMERA_DOWN</a>";
  
  s += "</center></html>\n";
  // Send the response to the client
  client.print(s);
  delay(1);
  Serial.println("Client disonnected");
}

String ipToString(IPAddress ip){
  String s="";
  for (int i=0; i<4; i++)
    s += i  ? "." + String(ip[i]) : String(ip[i]);
  return s;
}


//CAMERA
int changeCameAngle(int num){

  Serial.println("changeCameAngle " + num);
  cameraAngle = cameraAngle + num;
  if(cameraAngle >maxAngle){
    cameraAngle = maxAngle;
  }
  if(cameraAngle < minAngle){
    cameraAngle = minAngle;
  }
  return cameraAngle;
}

void changeCameAngle2(boolean up){

  if(up){
      for (int i = 0; i <= servoStep; i++) {
      servo.write(changeCameAngle(1));
      delay(servoDelay);
    }
  }else{
      for (int i = 0; i <= servoStep; i++) {
      servo.write(changeCameAngle(-1));
      delay(servoDelay);
    }
  };
}



//time
void allTimeToNull(){
  upTime = 0;
  downTime = 0;
  leftTime = 0;
  rightTime = 0;
}


//move methods

void moveStop(){
    digitalWrite(rightForward, 0);
    digitalWrite(leftForward, 0);
    digitalWrite(rightBack, 0);
    digitalWrite(leftBack, 0); 
}

void moveUp(){
      digitalWrite(rightForward, 1);
      digitalWrite(leftForward, 1);
}

void moveDown(){
    digitalWrite(rightBack, 1);
    digitalWrite(leftBack, 1);
}

void moveLeft(){
    digitalWrite(rightForward, 1);
    digitalWrite(rightBack, 1);
}

void moveRight(){
    digitalWrite(leftForward, 1);
    digitalWrite(leftBack, 1);
}
